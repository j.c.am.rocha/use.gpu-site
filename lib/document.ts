import { Document } from './types';
import slugs from '../docs/slugs.json';

const SLUGS = slugs as any as Record<string, Document>;

export const getDocumentBySlug = (slug: string): Document => SLUGS[slug];
