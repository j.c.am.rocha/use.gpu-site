import { DocumentIndex, Document, DocumentMatch, DocumentNode, IdNode } from './types';
import { buildLookup, buildPaths, toDocumentTree } from './paths';

import index from '../docs/index.json';
const INDEX = index as any as DocumentIndex;
const {paths: PATHS, children: IDTREE} = buildPaths(INDEX);
const {slugs: SLUGS, titles: TITLES} = buildLookup(INDEX);
const DOCTREE = toDocumentTree(INDEX, SLUGS);

export const getAllIndexedDocuments = (): Document[] => index.indexed;
export const getIndexedDocumentTree = (): DocumentNode[] => DOCTREE;
export const getIndexedDocumentByTitle = (title: string): Document | null => TITLES.get(title) ?? null;
export const getIndexedDocumentBySlug = (slug: string): Document => SLUGS.get(slug)!;
export const getPathBySlug = (slug: string): string[] => PATHS.get(slug) ?? [];
export const getChildren = (slug: string): string[] => IDTREE.get(slug) ?? [];

export const getIndexedDocumentsByRegExp = (re: RegExp): Document[] => {
  const out: Document[] = [];
  for (const doc of getAllIndexedDocuments()) {
    const {title, empty} = doc;
    if (title.match(re)) out.push(doc);
  }
  out.sort((a, b) => (b.score - a.score) || (a.title.localeCompare(b.title)));
  return out;
};

export const findIndexedDocuments = (key: string): DocumentMatch[] => {
  const k = key.toLowerCase();
  const ks = k.split(/\s+/g);

  const out: DocumentMatch[] = [];
  const seen = new Set<string>();

  for (const doc of getAllIndexedDocuments()) {
    const {title, empty} = doc;
    
    const t = title.toLowerCase();
    const i = t.indexOf(k);
    const titleMatch = i >= 0;

    let kw = 0;
    if (doc.keywords) for (const keyword of doc.keywords) {
      for (let k of ks) {
        if (keyword.indexOf(k) === 0) kw++;
      }
    }

    let boost = (titleMatch ? 10 : 0) + kw;
    
    if (titleMatch || kw) {
      if (empty) {
        const children = getChildren(doc.slug);
        for (const slug of children) {
          const doc = getIndexedDocumentBySlug(slug);
          if (!seen.has(doc.slug)) {
            seen.add(doc.slug);
            out.push({
              doc,
              path: getPathBySlug(doc.slug),
              score: doc.score + boost,
            });
          }
        }
      }
      else {
        if (!seen.has(doc.slug)) {
          const isExact = t === k;
          const isPrefix = i === 0;
          const score = isExact ? 10 : isPrefix ? 5 : 1;

          seen.add(doc.slug);
          out.push({
            doc,
            path: getPathBySlug(doc.slug),
            score: score + doc.score + boost,
          });
        }
      }
    }
  }

  out.sort((a, b) => (b.score - a.score) || (a.doc.title.localeCompare(b.doc.title)));

  return out.slice(0, 50);
};

