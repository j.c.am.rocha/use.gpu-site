import remarkParse from 'remark-parse';
import remarkRehype from 'remark-rehype';
import rehypeParse from 'rehype-parse';
import rehypeRaw from 'rehype-raw'
import rehypeHighlight from 'rehype-highlight';
import rehypeSanitize from 'rehype-sanitize';
import rehypeStringify  from 'rehype-stringify';
import {unified} from 'unified';
import {decode} from 'html-entities';

import {visitParents} from 'unist-util-visit-parents';
import {visit} from 'unist-util-visit';
import {u} from 'unist-builder';

import defaultSchema from 'hast-util-sanitize/lib/schema';
import merge from 'deepmerge';

import { getIndexedDocumentByTitle, getIndexedDocumentsByRegExp } from './indexed';

export const markdownToHTML = async (markdown: string) => {
  const html = (await unified()
    .use(remarkParse)
    .use(remarkRehype, {allowDangerousHtml: true})
    .use(rehypeRaw)
    .use(rehypeSanitize, schema as any)
    .use(rehypeHighlight, {plainText: ['wgsl', 'glsl']})
    .use(rehypeCodeReferences)
    .use(rehypeStringify as any)
    .process(markdown)
  ).toString();

  const chapters: [string, number][] = [];
  await unified()
    .use(rehypeParse)
    .use(rehypeHeaders, {chapters})
    .process(html)

  return [html, chapters];
};

function rehypeHeaders(config: {chapters: [string, number][]}) {
  const {chapters} = config;

  // @ts-ignore
  this.Compiler = (tree: any) => {
    const toText = (node: any) => {
      const out: string[] = [];
      visit(node, 'text', (node: any) => out.push(node.value));
      return out.join('');
    };

    visit(tree, 'element', (node: any) => {
      if (node.tagName === 'h1') chapters.push([toText(node), 1]);
      else if (node.tagName === 'h2') chapters.push([toText(node), 2]);
      else if (node.tagName === 'h3') chapters.push([toText(node), 3]);
      else if (node.tagName === 'h4') chapters.push([toText(node), 4]);
    });
  };
};

const rehypeCodeReferences = () => {

  const replaceNode = (parent: any, node: any, ...nodes: any[]) => {
    const {children} = parent;
    const i = children.indexOf(node);
    if (i >= 0) {
      children.splice(i, 1, ...nodes);
    }
  };

  return (tree: any) => {
    visitParents(tree, 'element', (node: any, ancestors: any[]) => {
      const className = node?.properties?.className ?? [];

      const isCode = node.tagName === 'code';
      const isHighlighted = className.filter((c: string) => c.match(/language/)).length;
      
      let isRef = false;
      let isRefs = false;
      if (node.children?.length) {
        const [{value}] = node.children;
        isRef = !!(value && value.match(/@{<?([^>}]+)>?}/));
        isRefs = !!(value && value.match(/@refs{<?([^>}]+)>?}/));
      }

      if (isRefs) visitParents(node, 'text', (node: any, ancestors: any[]) => {
        const parent = ancestors[ancestors.length - 1];
        let refs = node.value;
        let re = /@refs{([^}]+)}/;

        const out: any[] = [];
        
        let m: any;
        while (m = refs.match(re)) {
          const i = m.index;
          const ref = m[0].slice(6, -1);
          out.push(u('text', {value: refs.slice(0, i)}));

          const re = new RegExp(ref);
          const docs = getIndexedDocumentsByRegExp(re);

          const list = u('element',
            {tagName: 'ul'},
            docs.map((doc) => {
              const {href} = doc;
              return u('element', {tagName: 'li'}, [
                u('element', {tagName: 'a', properties: {href}}, [
                  u('text', {value: doc.title})
                ])
              ]);
            }),
          );
          out.push(list);

          refs = refs.slice(i + m[0].length);
        }
        out.push(u('text', {value: refs}));
                  
        replaceNode(parent, node, ...out);
      });

      if (isCode && (isHighlighted || isRef)) visitParents(node, 'text', (node, ancestors) => {
        const parent = ancestors[ancestors.length - 1];

        const className = parent?.properties?.className;
        if (className) {
          if (
            (className.indexOf('hljs-comment') >= 0) ||
            (className.indexOf('hljs-attr') >= 0)
          ) return;
        }

        let ref = node.value;
        
        const r = ref.match(/@{(<?[^>}]+>?)}/);
        if (r) {
          node.value = r[1];
          ref = r[1].replace(/[<>]/g, '');
        }

        const doc = getIndexedDocumentByTitle(ref);
        if (doc) {
          const {href} = doc;
          replaceNode(parent, node, u('element', {tagName: 'a', properties: {href}}, [node]));
        }
        
        if (ref.match(/\s/)) {
          const tokens = ref.split(/(?=[\s,()\[\]<>:])|(?<=[\s,()\[\]<>:])/).map((ref: string) => {
            const node = {type: 'text', value: ref};
            const doc = getIndexedDocumentByTitle(ref);
            if (doc) {
              const {href} = doc;
              return u('element', {tagName: 'a', properties: {href}}, [node]);
            }
            return node;
          });

          replaceNode(parent, node, ...tokens);
        }
      });
    });

    return tree;
  }
}

const schema = {
  strip: ['script'],
  clobberPrefix: 'user-content-',
  clobber: ['name', 'id'],
  ancestors: {
    tbody: ['table'],
    tfoot: ['table'],
    thead: ['table'],
    td: ['table'],
    th: ['table'],
    tr: ['table']
  },
  protocols: {
    href: ['http', 'https', 'mailto', 'xmpp', 'irc', 'ircs'],
    cite: ['http', 'https'],
    src: ['http', 'https'],
    longDesc: ['http', 'https']
  },
  tagNames: [
    'h1',
    'h2',
    'h3',
    'h4',
    'h5',
    'h6',
    'br',
    'b',
    'i',
    'strong',
    'em',
    'a',
    'pre',
    'code',
    'img',
    'tt',
    'div',
    'ins',
    'del',
    'sup',
    'sub',
    'p',
    'ol',
    'ul',
    'table',
    'thead',
    'tbody',
    'tfoot',
    'blockquote',
    'dl',
    'dt',
    'dd',
    'kbd',
    'q',
    'samp',
    'var',
    'hr',
    'ruby',
    'rt',
    'rp',
    'li',
    'tr',
    'td',
    'th',
    's',
    'strike',
    'summary',
    'details',
    'caption',
    'figure',
    'figcaption',
    'abbr',
    'bdo',
    'cite',
    'dfn',
    'mark',
    'small',
    'span',
    'time',
    'wbr',
    'input',
    'iframe',
  ],
  attributes: {
    a: ['href'],
    iframe: ['src', 'frameBorder', 'allowFullScreen', 'style'],
    img: ['src', 'longDesc', 'alt'],
    input: [
      ['type', 'checkbox'],
      ['disabled', true]
    ],
    li: ['task-list-item'],
    div: ['itemScope', 'itemType'],
    blockquote: ['cite'],
    del: ['cite'],
    ins: ['cite'],
    q: ['cite'],
    '*': [
      'abbr',
      'accept',
      'acceptCharset',
      'accessKey',
      'action',
      'align',
      'alt',
      'ariaDescribedBy',
      'ariaHidden',
      'ariaLabel',
      'ariaLabelledBy',
      'axis',
      'border',
      'cellPadding',
      'cellSpacing',
      'char',
      'charOff',
      'charSet',
      'checked',
      'className',
      'clear',
      'cols',
      'colSpan',
      'color',
      'compact',
      'coords',
      'dateTime',
      'dir',
      'disabled',
      'encType',
      'htmlFor',
      'frame',
      'headers',
      'height',
      'hrefLang',
      'hSpace',
      'isMap',
      'id',
      'label',
      'lang',
      'maxLength',
      'media',
      'method',
      'multiple',
      'name',
      'noHref',
      'noShade',
      'noWrap',
      'open',
      'prompt',
      'readOnly',
      'rel',
      'rev',
      'rows',
      'rowSpan',
      'rules',
      'scope',
      'selected',
      'shape',
      'size',
      'span',
      'start',
      'style',
      'summary',
      'tabIndex',
      'target',
      'title',
      'type',
      'useMap',
      'vAlign',
      'value',
      'vSpace',
      'width',
      'itemProp'
    ]
  },
  required: {
    input: {
      type: 'checkbox',
      disabled: true
    }
  }
};

