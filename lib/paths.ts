import { Document, DocumentIndex, DocumentNode, IdNode } from './types';

export const buildLookup = (index: DocumentIndex) => {
  const slugs: Map<string, Document> = new Map();
  const titles: Map<string, Document> = new Map();
  for (const doc of index.indexed) {
    slugs.set(doc.slug, doc);
    titles.set(doc.title, doc);
  }
  return {slugs, titles};
}

export const buildPaths = (index: DocumentIndex) => {
  const nodes: DocumentNode[] = [];
  const paths: Map<string, string[]> = new Map();
  const children: Map<string, string[]> = new Map();

  const traverse = (node: IdNode, path: string[] = []) => {
    const slug = node.props;
    const p = [...path, slug];
    paths.set(slug, p);

    if (node.children) {
      for (const n of node.children) {
        traverse(n, p);
      }
      children.set(slug, node.children.map(n => n.props));
    }
  };

  for (const n of index.nodes) {
    traverse(n, []);
  }

  return {paths, children};
};

export const toDocumentTree = (index: DocumentIndex, lookup: Map<string, Document>): DocumentNode[] => {
  const map = (node: IdNode): DocumentNode => {
    const doc = lookup.get(node.props)!;
    return {
      props: doc,
      children: node.children ? node.children.map(n => map(n)) : undefined,
    };
  };

  return index.nodes.map(map);
};