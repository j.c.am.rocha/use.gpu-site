export type PackageFile = MetaFile;
export type MarkdownFile = MetaFile;
export type MetaFile = {
  file: string,
  meta: Record<string, any>,
  content: string,
};

export type TreeNode<T> = {
  props: T,
  children?: TreeNode<T>[],
};

export type Document = {
  title: string,
  content: string,
  icon: string,
  slug: string,
  href?: string,
  base?: string,

  namespace: string,
  score: number,

  order?: number,
  keywords?: string[],
  prefix?: React.ReactNode,
  open?: boolean,
  group?: boolean,
  empty?: boolean,
};

export type IdNode = TreeNode<string>;
export type IdTree = IdNode[];

export type DocumentNode = TreeNode<Document>;
export type DocumentTree = DocumentNode[];

export type DocumentIndex = {
  nodes: IdNode[],
  indexed: Document[],
};

export type DocumentMatch = {
  doc: Document,
  path: string[],
  score: number,
};

