import React, { FC } from 'react';
import { styled } from '../../lib/stitches.config';
import { GRID, STYLES, FONTS, COLORS } from './constants';

export const Padded = styled('div', {
  paddingLeft: GRID.m,
  paddingRight: GRID.m,
  paddingTop: GRID.s,
  paddingBottom: GRID.s,
});

export const Inset = styled('div', {
  marginLeft: -GRID.s,
  marginRight: -GRID.s,
});
