import React, { FC, Fragment } from 'react';
import { styled } from '../../lib/stitches.config';
import { COLORS, GRID } from './constants';
import { Cursor } from '../util/cursor';
import { DocumentTree, DocumentNode, Document } from '../../lib/types';
import { Expandable } from './expandable';
import Link from 'next/link';

const openIcon = 'expand_more';
const closedIcon = 'chevron_right';
const ICON = (s: string) => <span className="m-icon">{s}</span>;

const StyledTreeRow = styled('div', {
  paddingTop: GRID.s / 2,
  paddingBottom: GRID.s / 2,
  cursor: 'default',
  whiteSpace: 'nowrap',
  textOverflow: 'ellipsis',
  overflow: 'hidden',
  userSelect: 'none',
  '&:hover': {
    background: COLORS.bgHover,
  },
  variants: {
    active: {
      true: {
        fontWeight: 'bold',
        color: COLORS.textActive,
      },
    },
    selected: {
      true: {
        '&&': {
          background: COLORS.bgSelected,
        }
      },
    },
  },
});

const StyledTreeRowGroup = styled('div', {
  paddingTop: GRID.l + GRID.s / 2,
  paddingBottom: GRID.s / 2,
  cursor: 'default',
  userSelect: 'none',
  fontSize: '0.8em',
  fontWeight: 700,
  textTransform: 'uppercase',
});

const StyledInlineButton = styled('div', {
  display: 'inline-block',
  verticalAlign: 'middle',
  width: 24,
  height: 0,
  position: 'relative',
  top: -12,
});

const StyledClickZone = styled('div', {
  padding: 5,
  margin: -5,
});

const StyledDot = styled('div', {
  display: 'inline-block',
  width: 24,
  paddingRight: 3,
  textAlign: 'center',
  opacity: 0.5,
  variants: {
    selected: {
      true: {
        opacity: 1,
      },
    },
  },
});

const Muted = styled('span', {
  opacity: 0.5,
  variants: {
    selected: {
      true: {
        opacity: 0.6,
      },
    },
  },
});

type TreeProps = {
  expandCursor: Cursor<any>,

  outline?: DocumentTree,
  selected?: string[],
  indent?: number,
};

type TreeRowProps = {
  active?: boolean | null,
  selected?: boolean | null,
  expanded?: boolean | null,
  document: Document,
  indent?: number,
  onExpand?: (e: any) => void,
};

export const Tree: FC<TreeProps> = (props: TreeProps) => {
  const {
    expandCursor,
    outline,
    selected = [],
    indent = 0,
  } = props;

  return (<>
    {outline ? outline.map((node: DocumentNode, i: number) => {
      const {props, children} = node;
      const id = props.slug;

      const isPinned = props.open;
      const isActive = selected[0] === id;
      const isSelected = isActive && selected.length === 1;

      if (props.group) {
        return (<Fragment key={id}>
          <TreeRowGroup document={props} indent={indent} />
          <Tree
            expandCursor={expandCursor}
            outline={children}
            selected={selected.slice(1)}
            indent={indent}
          />
        </Fragment>);  
      }

      return children?.length ? (
        <Expandable key={id} id={id} expandCursor={expandCursor} initialValue={!!(isActive || isPinned)}>{
          (isExpanded: boolean, onClick: (e: any) => void) => <>
            <TreeRow
              expanded={isExpanded}
              active={isActive}
              selected={isSelected}
              indent={indent}
              document={props}
              onExpand={onClick}
            />
            {isExpanded && children.length ?
              <Tree
                expandCursor={expandCursor}
                outline={children}
                selected={selected.slice(1)}
                indent={indent + 1}
              />
            : null}
          </>
        }</Expandable>
      ) : (
        <TreeRow
          key={id}
          active={isActive}
          selected={isSelected}
          indent={indent}
          document={props}
        />
      );
    }) : null}
  </>);
};

export const TreeRow: FC<TreeRowProps> = (props: TreeRowProps) => {
  const {active, selected, expanded, document, indent = 0, onExpand} = props;
  const {icon, prefix, title, slug, href} = document;
  
  const expandIcon = expanded ? openIcon : closedIcon;
  const sigil = (expanded != null
    ? <StyledInlineButton><StyledClickZone onClick={onExpand}>{ICON(expandIcon)}</StyledClickZone></StyledInlineButton>
    : icon !== 'description'
      ? <StyledInlineButton>{ICON(icon)}</StyledInlineButton>
      : active
        ? <StyledDot selected={active}>•</StyledDot>
        : <StyledDot>·</StyledDot>
  );
  
  const className = selected ? 'row-selected' : undefined;

  const row = (
    <StyledTreeRow className={className} style={{paddingLeft: indent * GRID.m + GRID.s}} selected={!!selected} active={!!active} onClick={href ? undefined : onExpand}>
      {sigil} <Muted selected={!!active}>{prefix}</Muted>{title}
    </StyledTreeRow>
  );

  return href ? (<Link href={href}><a href={href}>{row}</a></Link>) : row;
};

export const TreeRowGroup: FC<TreeRowProps> = (props: TreeRowProps) => {
  const {document, indent = 0} = props;
  const {prefix, title, slug, href} = document;
  
  const row = (
    <StyledTreeRowGroup style={{paddingLeft: indent * GRID.m + GRID.s}}>
      <Muted>{prefix}</Muted>{title}
    </StyledTreeRowGroup>
  );

  return row;
};
