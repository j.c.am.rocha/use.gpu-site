export const COLORS = {
  bgMasthead: '#404040',
  bgPanel: '#303030',
  bgHover: '#404040',
  bgKey: '#505050',
  bgButton: '#606060',
  bgButtonHover: '#707070',
  bgPrimary: 'rgba(120, 202, 255, 0.5)',
  bgPrimaryHover: 'rgba(120, 202, 255, 0.65)',
  bgHighlighted: 'rgba(0, 200, 255, 0.3)',
  bgSelectedFade: 'rgba(192, 192, 192, 0.1)',
  bgSelected: 'rgba(0, 200, 255, 0.3)',
  bgShine: 'rgba(255, 255, 255, 0.1)',
  bgBody: '#202020',
  
  textHeader: '#fff',
  textBrand: '#a0e7ff',
  textActive: '#fff',
  textBody: '#ddd',
  textMuted: '#999',
  
  borderThin: '1px solid #444',
  borderFocus: '2px solid #555',
  borderDivider: '1px solid #333',
};

export const GRID = {
  s: 8,
  m: 16,
  l: 32,
};

export const STYLES = {
  paddedLeft: { paddingLeft: GRID.m },
  paddedRight: { paddingRight: GRID.m },
  paddedLeftRight: { paddingLeft: GRID.m, paddingRight: GRID.m },
  
  shadowLow: { boxShadow: '0 5px 10px rgba(0, 0, 0, .15)' },
  shadowMedium: { boxShadow: '0 5px 10px rgba(0, 0, 0, .25)' },
  shadowHigh: { boxShadow: '0 5px 10px rgba(0, 0, 0, .5)' },

  shadowKey: { boxShadow: '0 2px 1px rgba(0, 0, 0, .5), inset 0 1px 2px rgba(255, 255, 255, .25)' },
};

export const FONTS = {
  headerPage: {
    fontSize: '24px',
    lineHeight: '28px',
  },
  bodyText: {
    fontSize: '16px',
    lineHeight: '24px',
  },
};
