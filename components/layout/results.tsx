import React, { FC, Fragment } from 'react';
import { styled } from '../../lib/stitches.config';
import { COLORS, GRID } from './constants';
import { Cursor } from '../util/cursor';
import { DocumentTree, DocumentNode, Document } from '../../lib/types';
import { DocumentMatch } from '../../lib/types';
import Link from 'next/link';

const ICON = (s: string) => <span className="m-icon">{s}</span>;

const StyledResultRow = styled('div', {
  paddingTop: GRID.s / 2,
  paddingBottom: GRID.s / 2,
  cursor: 'default',
  userSelect: 'none',
  whiteSpace: 'nowrap',
  textOverflow: 'ellipsis',
  overflow: 'hidden',
  '&:hover': {
    background: COLORS.bgHover,
  },
  variants: {
    active: {
      true: {
        '&&&': {
          background: COLORS.bgHighlighted,
        }
      },
    },
    selected: {
      true: {
        '&&': {
          fontWeight: 700,
          background: COLORS.bgSelected,
        }
      },
    },
    deemph: {
      true: {
        '&&': {
          background: COLORS.bgSelectedFade,
        }
      }
    }
  },
});

const StyledResultGroup = styled('div', {
  paddingTop: GRID.s / 2,
  paddingBottom: GRID.s / 2,
  cursor: 'default',
  userSelect: 'none',
  fontSize: '0.8em',
  fontWeight: 700,
  textTransform: 'uppercase',
});

const StyledIcon = styled('div', {
  position: 'relative',
  display: 'inline-block',
  top: 3,
  width: 24,
  marginLeft: GRID.s,
});

const Namespace = styled('span', {
  opacity: 0.5,
  float: 'right',
  fontSize: '0.8em',
  position: 'relative',
  top: 2,
  paddingLeft: GRID.s,
  paddingRight: GRID.s,
});

const Muted = styled('span', {
  opacity: 0.5,
  variants: {
    selected: {
      true: {
        opacity: 0.6,
      },
    },
  },
});

type ResultsProps = {
  matches?: DocumentMatch[],
  active?: string,
  selected?: string,
};

type ResultRowProps = {
  active?: boolean | null,
  selected?: boolean | null,
  deemph?: boolean | null,
  document: Document,
  onClick?: (e: any) => void,
};

export const Results: FC<ResultsProps> = (props: ResultsProps) => {
  const {
    matches,
    active = '',
    selected = '',
  } = props;

  return (<>
    <StyledResultGroup>Results</StyledResultGroup>
    {matches ? matches.map((match: DocumentMatch) => {
      const {doc} = match;
      const id = doc.slug;

      const isActive = active === id;
      const isSelected = selected === id;
      const isDeemph = active != '' && isSelected;
      
      return (
        <a key={id}>
          <ResultRow
            selected={isSelected}
            deemph={isDeemph}
            active={isActive}
            document={doc}
          />
        </a>
      );
    }) : null}
  </>);
};

export const ResultRow: FC<ResultRowProps> = (props: ResultRowProps) => {
  const {document, active, deemph, selected} = props;
  const {prefix, title, slug, href, icon, namespace} = document;
  
  const sigil = <StyledIcon>{ICON(icon ?? "description")}</StyledIcon>;
  const className = active ? 'row-selected' : undefined;

  const row = (
    <StyledResultRow className={className} selected={!!selected} active={!!active} deemph={!!deemph}>
      <Namespace>{namespace}</Namespace>{sigil} <Muted selected={!!active}>{prefix}</Muted>{title}
    </StyledResultRow>
  );

  return href ? (<Link href={href}>{row}</Link>) : row;
};
