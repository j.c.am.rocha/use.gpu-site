import React, { FC, useRef, useEffect, useState } from 'react';
import { styled } from '../lib/stitches.config';
import { STYLES, GRID, COLORS } from './layout/constants';
import { useRefineCursor, Cursor } from './util/cursor';
import { SearchState } from './outline';
import { DocumentMatch } from '../lib/types';

const ICON = (s: string) => <span className="m-icon">{s}</span>;

type Props = {
  matches: DocumentMatch[],
  searchCursor: Cursor<SearchState>,
  onSubmit: (match: DocumentMatch) => void,
};

export const Container = styled('div', {
  marginBottom: GRID.m,
  position: 'sticky',
  top: 0,
  paddingTop: GRID.m,
  background: COLORS.bgPanel,
  boxShadow: `0 0 5px ${GRID.s + 2}px ${COLORS.bgPanel}`,
  zIndex: 5,

  "@media (max-width: 768px)": {
    paddingTop: GRID.s,
    marginTop: '1px',
    marginLeft: GRID.l * 1.25,
  },
});

export const SearchIconLeft = styled('div', {
  position: 'absolute',
  top: 6 + GRID.m,
  left: '3px',
  textAlign: 'center',
  width: '32px',
  color: '#bbb',

  "@media (max-width: 768px)": {
    top: 6 + GRID.s,
  }
});

export const SearchIconRight = styled('div', {
  position: 'absolute',
  top: 6 + GRID.m,
  right: '3px',
  textAlign: 'center',
  width: '32px',
  color: '#bbb',
  cursor: 'pointer',

  "@media (max-width: 768px)": {
    top: 6 + GRID.s,
  }
});

export const SearchKeyRight = styled('div', {
  position: 'absolute',
  top: 5 + GRID.m,
  right: '6px',
  textAlign: 'center',
  width: '20px',
  color: '#bbb',
  opacity: 0.5,
  transition: 'opacity 0.15s ease-in-out',

  fontWeight: 'bold',
  fontSize: '12px',
  lineHeight: '22px',
  background: COLORS.bgKey,
  ...STYLES.shadowKey,
  
  '&.focused': { opacity: 0 },
  cursor: 'default',

  "@media (max-width: 768px)": {
    top: 6 + GRID.s,
  }
});

export const SearchInput = styled('input', {
  height: GRID.l,
  lineHeight: GRID.l,
  paddingLeft: GRID.l + 3,
  paddingRight: GRID.l + 3,
  width: '100%',
  font: 'inherit',
  color: 'inherit',
  background: 'rgba(0, 0, 0, .1)',
  border: COLORS.borderThin,
  borderRadius: '1px',
  
  '&:focus': {
    border: COLORS.borderFocus,
    borderRadius: '2px',
    margin: '-1px',
    outline: 'none',
    height: GRID.l + 2,
  }
});

type StringChange = (s: string) => void;
const makeHandleChange = (f: StringChange) => (e: any) => f(e.target.value);

export const Search: FC<Props> = (props: Props) => {
  const {matches, onSubmit, searchCursor} = props;

  const useSearchCursor = useRefineCursor(searchCursor);
  const [text, updateText] = useSearchCursor<string>('query');
  const [index, updateIndex] = useSearchCursor<number>('selectedIndex');
  const [focused, updateFocused] = useSearchCursor<boolean>('focused');
  
  const handleMove = (offset: number) => {
    updateIndex({$apply: (i: number) => i + offset});
  };
  
  const handleChange = makeHandleChange((s: string) => {
    updateText(s);
  });

  const handleClear = () => updateText("");
  
  const handleSubmit = (e: any) => {
    if (!onSubmit) return;
    if (e) {
      e.preventDefault();
      e.stopPropagation();
    }
    
    const i = Math.max(0, Math.min(matches.length - 1, index));
    const match = matches[i];
    if (match) onSubmit(match);
  };

  const handleFocus = (e: any) => {
    updateFocused(true);
  }

  const handleBlur = (e: any) => {
    updateFocused(false);
  }

  const inputRef = useRef<HTMLInputElement>(null);
  const handleAutoFocus = (e?: any) => {
    if (e) {
      e.preventDefault();
      e.stopPropagation();
    }

    if (window.innerWidth > 768) {
      const {current: input} = inputRef;
      if (input) input.focus();
    }
  }

  const handleKeyDown = (e: any) => {
    if (e.key === 'ArrowDown') handleMove(1);
    else if (e.key === 'ArrowUp') handleMove(-1);
    else if (e.key === 'Tab' || e.key === 'Enter') handleSubmit(e);
    else if (e.key === 'Escape') {
      if (text !== '') updateText('');
      else inputRef.current?.blur();
    }
    else return;

    e.preventDefault();
    e.stopPropagation();
  };

  useEffect(() => {
    const handleKeyDown = (e: any) => {
      if ((e.key === '/') || (e.key === 'k' && (e.ctrlKey || e.metaKey))) {
        if (document.activeElement == document.body) {
          handleAutoFocus();
          e.preventDefault();
          e.stopPropagation();
        }
      }
    };
    
    document.addEventListener('keydown', handleKeyDown, true);
    return () => { document.removeEventListener('keydown', handleKeyDown, true); }
  }, []);

  const containerRef = useRef<HTMLDivElement>(null);
  useEffect(() => {
    const {current: container} = containerRef;

    const handleScroll = () => {
      if (!container) return;

      let l = '';
      if (window.innerWidth <= 768) {
        const y = (window.scrollY || document.body.scrollTop);
        const f = Math.min(Math.max(0, y - 5), 40);
        const e = .5 - .5 * Math.cos(f/40 * Math.PI);

        l = Math.round(e * GRID.l * 1.25) + 'px';
      }
      if (container.style.marginLeft !== l) container.style.marginLeft = l;
    };
    
    handleScroll();
    
    window.addEventListener('resize', handleScroll, true);
    window.addEventListener('scroll', handleScroll, true);
    return () => {
      document.removeEventListener('resize', handleScroll, true);
      document.removeEventListener('scroll', handleScroll, true);
    }
  }, []);

  return (
    <Container ref={containerRef}>
      <SearchIconLeft onMouseDown={handleAutoFocus}>{ICON('search')}</SearchIconLeft>
      {text.length
        ? <SearchIconRight onClick={handleClear}>{ICON('clear')}</SearchIconRight>
        : <SearchKeyRight className={focused ? 'focused' : ''} title="Press '/' to search">/</SearchKeyRight>}
      <SearchInput
        ref={inputRef}
        type="text"
        value={text}
        onBlur={handleBlur}
        onFocus={handleFocus}
        onChange={handleChange}
        onKeyDown={handleKeyDown}
        placeholder="Search…"
      />
    </Container>
  )
};