import React from 'react';
import Link from 'next/link';

import type { NextPage } from 'next';

type Props = object;

const QuothTheServer: NextPage<Props> = (props: Props) => {

  if (typeof location !== 'undefined') {
    const {pathname} = location;
    if (pathname.match(/\/demo/)) {
      const path = '/demo/index.html#!' + location.pathname.replace('/demo/', '/');
      location.href = path;
    }
    return <div />;
  }

  return (<div style={{opacity: 0}}>
    <h1>404 - Page Not Found</h1>
    <Link href="/">
      <a>
        Go back home
      </a>
    </Link>
  </div>);
}

export default QuothTheServer;
