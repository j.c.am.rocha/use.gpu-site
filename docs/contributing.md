---
title: Contributing
order: 4
---

# Contributing

Contributors and other interested parties are welcome, but should not expect production-ready code and documentation.

There are a **bunch of examples** included in the [demo app](/docs/reference-dev-@use-gpu-app), which you can explore and tweak to become familiar with the library.

Use.GPU is **still an alpha**, but significant parts of its API have already stabilized. Until it reaches 1.0, breaking changes should be expected, but will be documented.

**Not all contributions need to be in the main repo**. Use.GPU heavily encourages using modules to assemble your solution à la carte. The built-in workbench components are just one way of doing things.

**Questions? [Join Use.GPU Discord](https://discord.gg/WxtZ28aUC3)**

## Ideas

Here are some projects and initiatives that would be cool to work on:

### Tooling

#### Web build systems and guides
Use.GPU has mainly been tested on `webpack` with `babel`. Other build systems and transpilers are untested or unsupported. New loader plug-ins, and/or guides on how to configure them for a certain target, are most welcome.

### Use Cases

#### Data viz examples
The data-driven geometry components are fully functional, suitable for both technical and mathematical use. Building or replicating common visualizations as examples would help a lot, both to test the library and to figure out what to prioritize.

#### Shader toy
A shader toy-like environment for WGSL / WebGPU, using the shader linker to do all the hard work. See if you can use the built-in render-to-texture components. RTT hasn't been used for anything really complicated, so this will likely reveal a few blind spots.

### Components

#### Vector map tiles
Basic Mapbox Vector Tiles support exists in `@use-gpu/map` in alpha form. It can render styled lines and polygons, but no text or markers. This package also needs to be extended to allow control over what is rendered and how.

#### MathBox³
Use.GPU Plot is a continuation of MathBox, though only some of its components have been ported so far. A few nice pieces are missing: the set of 4D views, the array slicing/swizzling/resampling operators, etc. Use.GPU also allows for a much better implementation of e.g. presentation slides. Port MathBox 2 - The Good Parts to Use.GPU.

## Repo Orientation

The repo is structured as a Yarn Workspace:

**Code**
- `/examples` - Example scaffolds
- `/packages` - Main TS packages
- `/public` - Assets for demo app
- `/rust` - WASM/Rust code

**Maintenance**
- `/build` - Build output (generated)
- `/scripts` - Build scripts
- `/test` - Integration tests

## Building

The repo is a typical TS → JS build, but with additional requirements:

- Support both ES modules and CommonJS
- Import `.wgsl` shaders into TS modules
- Include a Rust/WASM module (`@use-gpu/glyph`)

This is why the build process is pretty involved.

**During development**, the top-level `webpack` server can build the entire workspace directly from the TS source. This allows for hot-reload.
This is the easiest way (`yarn dev`).

**To build the final packages:**
- A single top-level `yarn build` dispatches a `yarn build` of each package
- Each package builds itself in `/build/packages/$pkg` according to its own `package.json`
- This uses the shared build scripts in `/scripts`:
  - Typescript is compiled to JS in `/build/ts/$pkg`.
  - Output is copied to `/build/packages/$pkg/mjs/` (ESM) + `/build/packages/$pkg/cjs/` (CommonJS)
  - The `package.json` is built from the source, with TS references/exports replaced by JS.

Exceptions:
- `@use-gpu/wgsl` contains shaders in `.wgsl` source form. It has a custom build step to generate the matching `.d.ts` files.

