---
title: Roadmap
order: 3
---

# Roadmap

**Use.GPU is in the alpha stage**.

The goal is to have a viable 1.0 around the same time [WebGPU becomes generally usable](https://github.com/gpuweb/gpuweb/milestone/2).

It also has first-class React support, in both directions. This is already working in [@use-gpu/react](/docs/reference-components-@use-gpu-react).

## Features

Use.GPU aims to provide a decent baseline solution for:
- ✅ *Plot* - Drawing points, lines, polygons, surfaces
- ⏱ *GLTF* - Standard PBR with texture maps and lighting
- ✅ *Data* - Simple, chunked and composite data with auto-aggregation
- ⏱ *Map* - Maps with 2D vector tiles
- ✅ *Sim* - Kernel-based grid simulations
- ✅ *Implicit* - Contouring / Raymarching
- ⏱ *FX* - First-class multi-pass effects pipeline
- ✅ *2D UI* - Layout, rich text, SDF shapes, input events
- ✅ *3D UI* - GPU picking, perspective transforms

This also includes some typical conveniences from the React ecosystem (like a URL router).

The core set of user-space functionality lives under `@use-gpu/workbench`, but some of this may end up split into separate packages.

- ✅: Stable Beta
- ⏱: Evolving Alpha
