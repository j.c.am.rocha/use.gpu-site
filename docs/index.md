---
title: Intro
order: 0
---

<span className="info-box">
  <span className="m-icon m-icon-outlined" title="Info">info</span>This documentation is a work-in-progress. Use.GPU is in alpha.
</span>

<span className="warning-box">
  <span className="m-icon m-icon-outlined" title="Warning">warning_amber</span>WebGPU is <a href="https://caniuse.com/webgpu">only available for developers</a>, locked behind a browser flag. A dev build of Chrome or Firefox is recommended.</a>.
</span>

# Intro

Use.GPU is a set of **declarative, reactive WebGPU legos**. Compose live graphs, layouts, meshes and shaders, on the fly.

It's a **stand-alone Typescript+Rust/WASM library** with its own React-like run-time. If you're familiar with React, you will feel right at home.

It has a built-in **shader linker and binding generator**, which means a lot of the tedium of common GPU programming is eliminated, without compromising on flexibility.

**Questions? [Join Use.GPU Discord](https://discord.gg/WxtZ28aUC3)**

<div className="demo-button"><a href="/demo/index.html" target="_blank"><span>View Demos</span></a></div>

<div style="display: flex; flex-wrap: wrap;">
  <div style="width: 50%; padding: 5px">
    <a href="/demo/plot/cartesian"><img src="/images/example1.jpg" alt="Example - 3D Surface Plot" style="display: block; width: 100%"></a>
  </div>
  <div style="width: 50%; padding: 5px">
    <a href="/demo/geometry/gltf"><img src="/images/example2.jpg" alt="Example - GLTF Space Helmet" style="display: block; width: 100%"></a>
  </div>
  <div style="width: 50%; padding: 5px">
    <a href="/demo/scene/shadow"><img src="/images/example7.jpg" alt="Example - Scene with Shadows" style="display: block; width: 100%"></a>
  </div>
  <div style="width: 50%; padding: 5px">
    <a href="/demo/geometry/data"><img src="/images/example4.jpg" alt="Example - 3D Line Grid" style="display: block; width: 100%"></a>
  </div>
  <div style="width: 50%; padding: 5px">
    <a href="/demo/rtt/cfd-compute"><img src="/images/example5.jpg" alt="Example - Fluid Dynamics Simulation" style="display: block; width: 100%"></a>
  </div>
  <div style="width: 50%; padding: 5px">
    <a href="/demo/map/webmercator"><img src="/images/example6.jpg" alt="Example - WebMercator Globe" style="display: block; width: 100%"></a>
  </div>
</div>

## Principle

Use.GPU lets you build **incremental apps**, which respond to arbitrary changes with **minimal recomputation**.

Similar to React, you use it by composing a tree of components, starting with an `<App>`:

```html
<App>
  <Router>
    <Routes>
      <MyPage>
        
        <WebGPU>
          <AutoCanvas>

            <Pass>
            </Pass>

          </AutoCanvas>
        </WebGPU>
        
      </MyPage>
    </Routes>
  </Router>
</App>
```

You can nest the Use.GPU components to create complex GPU graphics, with bespoke rendering pipelines. No heavy lifting required.

<div style="position: relative; width: 100%; padding-bottom: 56%; margin: 48px 0;">
  <iframe frameborder="0" allowfullscreen="allowfullscreen" style="position: absolute; top: 0; left: 0; right: 0; bottom: 0; width: 100%; height: 100%; border: 0;" src="https://www.youtube.com/embed/4cTSSAMlIY0"></iframe>
</div>

## Guides (pending)

 - [Getting started](/docs/guides-getting-started)
 - [WebGPU canvas](/docs/guides-webgpu-canvas)
 - [Drawing and animation](/docs/guides-drawing-animation)
 - [Scene geometry](/docs/guides-scene-geometry)
 - [2D and 3D plot](/docs/guides-2d-3d-plot)
 - [Data-driven geometry](/docs/guides-data-driven-geometry)
 - [Live vs React](/docs/guides-shaders)
 - [Memoization](/docs/guides-memoization)
 - [WGSL Shaders](/docs/guides-shaders)
 - [Renderers and Passes](/docs/guides-renderers-and-passes)
 - Layout and UI

## Packages

Use.GPU is divided into packages, at different levels of abstraction.
This enables free-form tinkering for any graphics skill level.

**Components**
- [`@use-gpu/gltf`](/docs/reference-components-@use-gpu-gltf) - GLTF loader and bindings
- [`@use-gpu/inspect`](/docs/reference-components-@use-gpu-layout) - Live inspector
- [`@use-gpu/layout`](/docs/reference-components-@use-gpu-layout) - HTML-like layout
- [`@use-gpu/map`](/docs/reference-components-@use-gpu-map) - Maps and projections
- [`@use-gpu/plot`](/docs/reference-components-@use-gpu-plot) - 2D/3D plotting (axes, grids, curves, labels, transforms, …)
- [`@use-gpu/react`](/docs/reference-components-@use-gpu-react) - Live ↔︎ React portals
- [`@use-gpu/scene`](/docs/reference-components-@use-gpu-scene) - Classic scene tree with instancing
- [`@use-gpu/webgpu`](/docs/reference-components-@use-gpu-webgpu) - WebGPU canvas
- [`@use-gpu/workbench`](/docs/reference-components-@use-gpu-workbench)
  - `/animate` - Keyframe animation
  - `/camera` - Views and controls
  - `/compute` - Compute kernels and staging
  - `/data` - CPU → GPU data packing
  - `/interact` - GPU UI picking
  - `/layers` - Data-driven geometry
  - `/light` - Light and environment
  - `/material` - Physical materials
  - `/pass` - Composable render passes
  - `/primitives` - Programmable geometry
  - `/queue` - WebGPU sequencing
  - `/render` - Forward + Deferred renderer and RTT
  - `/router` - URL ↔︎ Page routing
  - `/shader` - Custom WGSL injection
  - `/text` - SDF text rendering

**Libraries**
- [`@use-gpu/core`](/docs/reference-library-@use-gpu-core) - Pure WebGPU + data helpers
- [`@use-gpu/glyph`](/docs/reference-library-@use-gpu-glyph) - Rust/WASM ABGlyph wrapper
- [`@use-gpu/shader`](/docs/reference-library-@use-gpu-shader) - WGSL shader linker and tree shaker
- `@use-gpu/wgsl` - Standard .wgsl library

**Live**
- [`@use-gpu/live`](/docs/reference-live-@use-gpu-live) - Effect run-time (React replacement)
- [`@use-gpu/state`](/docs/reference-live-@use-gpu-state) - Minimal state management
- [`@use-gpu/traits`](/docs/reference-live-@use-gpu-traits) - Composable prop archetypes

**Loaders**
- [`@use-gpu/glsl-loader`](/docs/reference-loader-@use-gpu-glsl-loader) - GLSL loader (webpack / node / rollup)
- [`@use-gpu/wgsl-loader`](/docs/reference-loader-@use-gpu-wgsl-loader) - WGSL loader  (webpack / node / rollup)
