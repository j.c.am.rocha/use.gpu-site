---
title: Getting started
order: 0
---

# Getting started

## Overview

Use.GPU is run via the Live run-time, similar to React.

There are two ways to use it:
- **Stand-alone HTML**
- **Embedded inside React**

**Note: If you just want to edit the [existing demos](/demo/index.html), see [`@use-gpu/app`](/docs/reference-dev-@use-gpu-app).**

## Stand-alone HTML

Use.GPU embedded in a basic HTML page.

**Examples**
- [`/examples/ts-webpack`](https://gitlab.com/unconed/use.gpu/-/tree/master/examples/ts-webpack) - Empty canvas with `webpack` and `babel`.

**Steps**
- Required packages are included (`@use-gpu/workbench`, `@use-gpu/webgpu`, `@use-gpu/wgsl-loader`)
- Webpack is configured to compile `.wgsl` using `@use-gpu/wgsl-loader`
- Webpack is configured with sync `.wasm` modules on
- Import the Live `React` shim to make `<JSX>` work correctly
- Call `render()` on your `<App />`

e.g.

```tsx
import React, { render } from '@use-gpu/live';

window.onload = async () => {
  const { App } = await import('./app');
  render(<App />);
```

The `await import(…)` on the outside allows synchronous `import * from '.wasm'` to work on the inside.

## Embedded inside React

Use.GPU has convenient React ↔︎ Live portals in both directions. See [`@use-gpu/react`](/docs/reference-components-@use-gpu-react) for more info.

**Steps**
- Add required packages to your project (`@use-gpu/react`, `@use-gpu/workbench`, `@use-gpu/webgpu`, `@use-gpu/wgsl-loader`).
- Configure your build system to convert `.wgsl` to JS using `@use-gpu/wgsl-loader`
- Configure your build system to support sync `.wasm` imports
- Mount a React `<LiveCanvas>` to insert a `<canvas>`. Will make a portal to Live.
- Render Live components inside, (e.g. `<AutoCanvas canvas={canvas} />`).

On the React side:

```tsx
import React, { FC } from 'react';
import { LiveCanvas } from '@use-gpu/react';

import { Component } from './component';

// This is a React component
const MyCanvas: FC = () => {
  return (
    <LiveCanvas>
      // These are Live children
      {(canvas) => <Component canvas={canvas} />}
    </LiveCanvas>
  );
}
```

On the Live side:

```tsx
import type { LC, PropsWithChildren } from '@use-gpu/live';

type ComponentProps = {
  canvas: HTMLCanvasElement,
};

// This is a Live component
export const Component: LC<ComponentProps> = (props: PropsWithChildren<ComponentProps>) => {
  const {canvas} = props;

  // ...
  return null;
};
```

## WGSL Loader

To use `@use-gpu/workbench`, your build process must convert the `.wgsl` files inside `@use-gpu/wgsl` to JS. This can be done using the included [`@use-gpu/wgsl-loader`](/docs/reference-loader-@use-gpu-wgsl-loader), if your build system is compatible (webpack, node or rollup).

e.g. webpack config:

```js
{
  module: {
    rules: [
      {
        test: /\.(ts|js)x?$/,
        exclude: [/node_modules/],
        loader: 'babel-loader',
      },
      {
        test: /\.wgsl$/i,
        use: ['@use-gpu/wgsl-loader'],
      },
    ],
  },
}
```

## Hot Module Reload

Live has support for hot module reload with `webpack-dev-server`.

In your `webpack.config.js`:

- Include the HMR client in your `entry` point:

```tsx
{
  entry: [
    './src/index.tsx',
    'webpack-dev-server/client/index.js?hot=true&live-reload=true',
  ],
}
```

- Enable HMR server

```tsx
{
  devServer: {
    // ...
    hot: true,
  },
}
```

- Wrap your top level `App` component in `hot(..., module)`:

```tsx
import React, { hot } from '@use-gpu/live';

export const App = hot(() => {
  // ...
}, module);
```

